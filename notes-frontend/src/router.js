import Vue from 'vue'
import Router from 'vue-router'
import HomeComponent from '@/views/Home';
import EditComponent from '@/components/note/Edit';
import CreateComponent from '@/components/note/Create';
import CreateChecklistComponent from '@/components/note/CreateChecklist';


Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', redirect: { name: 'home' } },
    { path: '/home', name: 'home', component: HomeComponent },
    { path: '/create/note', name: 'Create', component: CreateComponent },
    { path: '/edit/:id', name: 'Edit', component: EditComponent },
    { path: '/create/checklist', name: 'CreateChecklist', component: CreateChecklistComponent },
  ]
});