# Build a note management app using Nest.js, Vue.js and MongoDB

Application repo for a notes management application built with Nest.js, Vue.js and MongoDB.

## Getting Started
This prototype is divided into two separate sections. Namely the Backend ( Built with Nest.js) and the frontend
( Built with Vue.js ).

### Clone the repository
To easily set up the application, clone this repository which contains directory for both sections of the project ( i.e `notes-app-backend` and `notes-app-frontend`)

```bash
git clone https://nTrajkovik@bitbucket.org/nTrajkovik/nest-notes-app.git
```

## Change directory into the newly cloned project
```bash
cd nest-notes-app
```

## Backend
### Change directory into the backend
```bash
cd notes-app-backend
```

### Install backend dependencies

```bash
npm install
```

### MongoDB

Start mongoDB (or don't):

This app uses a free instance of mongodb on the cloud. You can use your own by changing the connection string.

### Run the application
Open another terminal and still within the `notes-app-backend` project directory run the application with:

```bash
npm run start
```

This will start the backend application on port `3000`.

## Frontend
Open another terminal from the `nest-notes-app` and navigate to the `notes-app-frontend` folder to setup the frontend

### Frontend dependencies
```bash
cd notes-app-frontend
npm install
```

### Run the frontend app

```bash
npm run serve
```

### Test the application
Finally open your browser and view the application on http://localhost:8080

## Prerequisites
 [Node.js](https://nodejs.org/en/), [Npm](https://www.npmjs.com/), [MongoDB](https://docs.mongodb.com/v3.2/installation/)

## Built With
[Nest.js](https://nestjs.com/)
[Vue.js](https://vuejs.org/)
[MongoDB]() 