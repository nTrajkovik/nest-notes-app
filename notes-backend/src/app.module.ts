import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { NoteModule } from './note/note.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb+srv://Math:math99@cluster-99-t9ebn.mongodb.net/test?retryWrites=true&w=majority', { useNewUrlParser: true }),
    NoteModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
