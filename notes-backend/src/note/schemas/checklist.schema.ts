import * as mongoose from 'mongoose';

export const ChecklistSchema = new mongoose.Schema({
    lines: [],
    created_at: { type: Date, default: Date.now }
})