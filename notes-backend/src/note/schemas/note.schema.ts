import * as mongoose from 'mongoose';

export const NoteSchema = new mongoose.Schema({
    description: String,
    created_at: { type: Date, default: Date.now }
})