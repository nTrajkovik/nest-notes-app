import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Note } from './interfaces/note.interface';
import { CreateNoteDTO } from './dto/create-note.dto';
import { Checklist } from './interfaces/checklist.interace';
import { CreateChecklistDTO } from './dto/create-checklist.dto';


@Injectable()
export class NoteService {
    constructor(@InjectModel('Note') private readonly noteModel: Model<Note>, 
    @InjectModel('Checklist') private readonly checklistModel: Model<Checklist>) { }
    

    // fetch all notes
    async getAllNotes(): Promise<Note[]> {
        const notes = await this.noteModel.find().exec();
        return notes;
    }

    // fetch all notes
    async getAllChecklists(): Promise<Checklist[]> {
        const notes = await this.checklistModel.find().exec();
        return notes;
    }

    // Get a single note
    async getNote(noteID): Promise<Note> {
        const note = await this.noteModel.findById(noteID).exec();
        return note;
    }

    // Get a single note
    async getChecklist(checklistID): Promise<Checklist> {
        const note = await this.checklistModel.findById(checklistID).exec();
        return note;
    }

    // post a single note
    async addNote(createNoteDTO: CreateNoteDTO): Promise<Note> {
        const newNote = await this.noteModel(createNoteDTO);
        return newNote.save();
    }

    // post a single note
    async addChecklist(createChecklistDTO: CreateChecklistDTO): Promise<Checklist> {
        const newNote = await this.checklistModel(createChecklistDTO);
        return newNote.save();
    }

    // Edit note details
    async updateNote(noteID, createNoteDTO: CreateNoteDTO): Promise<Note> {
        const updatedNote = await this.noteModel
            .findByIdAndUpdate(noteID, createNoteDTO, { new: true });
        return updatedNote;
    }

    // Edit note details
    async updateChecklist(checklistID, createChecklistDTO: CreateChecklistDTO): Promise<Checklist> {
        const updatedNote = await this.checklistModel
            .findByIdAndUpdate(checklistID, createChecklistDTO, { new: true });
        return updatedNote;
    }

    // Delete a note
    async deleteNote(noteID): Promise<any> {
        const deletedNote = await this.noteModel.findByIdAndRemove(noteID);
        return deletedNote;
    }

    
    // Delete a checklist
    async deleteChecklist(checklistID): Promise<any> {
        const deletedNote = await this.checklistModel.findByIdAndRemove(checklistID);
        return deletedNote;
    }
}
