import { Document } from 'mongoose';

export interface Checklist extends Document {
    readonly lines: {}[];
    readonly created_at: Date;
}