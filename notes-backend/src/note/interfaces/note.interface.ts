import { Document } from 'mongoose';

export interface Note extends Document {
    readonly description: string;
    readonly created_at: Date;
}