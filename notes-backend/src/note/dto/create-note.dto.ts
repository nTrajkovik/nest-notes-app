export class CreateNoteDTO {
    readonly description: string;
    readonly created_at: Date;
}