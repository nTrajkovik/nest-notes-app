import { Controller, Get, Res, HttpStatus, Post, Body, Put, Query, NotFoundException, Delete, Param } from '@nestjs/common';
import { NoteService } from './note.service';
import { CreateNoteDTO } from './dto/create-note.dto';
import { CreateChecklistDTO } from './dto/create-checklist.dto';

@Controller('note')
export class NoteController {
    constructor(private noteService: NoteService) { }

    // Retrieve notes list
    @Get('notes')
    async getAllNotes(@Res() res) {
        const notes = await this.noteService.getAllNotes();
        return res.status(HttpStatus.OK).json(notes);
    }

    // Fetch a particular note using ID
    @Get('note/:noteID')
    async getNote(@Res() res, @Param('noteID') noteID) {
        const note = await this.noteService.getNote(noteID);
        if (!note) throw new NotFoundException('Note does not exist!');
        return res.status(HttpStatus.OK).json(note);

    }

    // Fetch a particular note using ID
    @Get('checklist/:checklistID')
    async getChecklist(@Res() res, @Param('checklistID') checklistID) {
        const note = await this.noteService.getChecklist(checklistID);
        if (!note) throw new NotFoundException('Note does not exist!');
        return res.status(HttpStatus.OK).json(note);

    }

    // add a note
    @Post('/create')
    async addNote(@Res() res, @Body() createNoteDTO: CreateNoteDTO) {
        const note = await this.noteService.addNote(createNoteDTO);
        return res.status(HttpStatus.OK).json({
            message: "Note has been created successfully",
            note
        })
    }

    // add a note
    @Post('/create/checklist')
    async addChecklist(@Res() res, @Body() createChecklistDTO: CreateChecklistDTO) {
        const note = await this.noteService.addChecklist(createChecklistDTO);
        return res.status(HttpStatus.OK).json({
            message: "Note has been created successfully",
            note
        })
    }
    
    // Retrieve notes list
    @Get('/checklists')
    async getAllChecklists(@Res() res) {
        const notes = await this.noteService.getAllChecklists();
        return res.status(HttpStatus.OK).json(notes);
    }

    // Update a note's details
    @Put('/checklist/update')
    async updateChecklist(@Res() res, @Query('checklistID') checklistID, @Body() createChecklistDTO: CreateChecklistDTO) {
        const note = await this.noteService.updateChecklist(checklistID, createChecklistDTO);
        if (!note) throw new NotFoundException('Note does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Note has been successfully updated',
            note
        });
    }

    
    // Update a note's details
    @Put('/update')
    async updateNote(@Res() res, @Query('noteID') noteID, @Body() createNoteDTO: CreateNoteDTO) {
        const note = await this.noteService.updateNote(noteID, createNoteDTO);
        if (!note) throw new NotFoundException('Note does not exist!');
        return res.status(HttpStatus.OK).json({
            message: 'Note has been successfully updated',
            note
        });
    }

    // Delete a note
    @Delete('/delete')
    async deleteNote(@Res() res, @Query('noteID') noteID) {
        const note = await this.noteService.deleteNote(noteID);
        if (!note) throw new NotFoundException('Note does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Note has been deleted',
            note
        })
    }

    // Delete a note
    @Delete('/checklist/delete')
    async deleteChecklist(@Res() res, @Query('checklistID') checklistID) {
        const note = await this.noteService.deleteChecklist(checklistID);
        if (!note) throw new NotFoundException('Note does not exist');
        return res.status(HttpStatus.OK).json({
            message: 'Note has been deleted',
            note
        })
    }
}
